//
//  Contact.swift
//  FunctionFrontendTableView
//
//  Created by Manuel Meyer on 14.03.22.
//

import Foundation

struct Contact:Identifiable, CustomStringConvertible {
    enum Change {
        case set(Set); enum Set {
            case family  (name:String)
            case personal(name:String) }
        case add(Add); enum Add {
            case address(Address) }
        case update(Update); enum Update {
            case address   (Address)
            case addresses([Address]) }
        case remove(Remove); enum Remove {
            case addresses(tagged:String)
            case tag       (named:String) }
    }
    let id          : UUID
    let familyName  : String?
    let personalName: String?
    let addresses   : [Address]
    
    init()                                  { self.init(UUID(),nil, nil, [])        }
    func alter(_ changes:Change...) -> Self { changes.reduce(self) { $0.alter($1) } }
    var description: String                 { "\(familyName ?? "")\(familyName != nil && personalName != nil ? ", " : "")\(personalName ?? "") \((addresses.count > 0) ? addresses.reduce("") { $0 + "\($1.description)" } : "")" }
}
private extension Contact {
    private init(_ id: UUID, _ familyName:String?, _ personalName:String?, _ adresses: [Address]) {
        self.id = id
        self.familyName = familyName
        self.personalName = personalName
        self.addresses = adresses
    }
    private func alter(_ change:Change) -> Self {
        switch change {
        case let .set   (.family   (  familyName)): return .init(id, familyName, personalName, addresses                                                             )
        case let .set   (.personal (personalName)): return .init(id, familyName, personalName, addresses                                                             )
        case let .add   (.address  (address     )): return .init(id, familyName, personalName, addresses                                                 + [address] )
        case let .update(.address  (address     )): return .init(id, familyName, personalName, addresses.filter { $0.id != address.id                  } + [address] )
        case let .update(.addresses(updated     )): return .init(id, familyName, personalName, addresses.filter { !addresses.map(\.id).contains($0.id) } + updated   )
        case let .remove(.addresses(tagged:tag  )): return .init(id, familyName, personalName, addresses.filter { !$0.tags.contains(tag)               }             )
        case let .remove(.tag      (       tag  )): return .init(id, familyName, personalName, addresses.map    { $0.alter(.remove(.tag(named:tag)))   }             )
        }
    }
}
