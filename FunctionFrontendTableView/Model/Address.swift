//
//  Address.swift
//  FunctionFrontendTableView
//
//  Created by Manuel Meyer on 14.03.22.
//

import Foundation

struct Address: Identifiable, Equatable {
    enum Change {
        case set(Set); enum Set {
            case street (to:String)
            case city   (to:String)
            case zip    (to:String)
            case country(to:String) }
        case add(Add); enum Add {
            case tag(name: String) }
        case remove(Remove); enum Remove {
            case tag(named:String) }
    }
    let id    : UUID
    let street: String?
    let city  : String?
    let country:String?
    let zip   : String?
    let tags  : [String]
    
    init(tag:String? = nil)                 { self.init(UUID(), nil, nil, nil, nil, tag != nil ? [tag!] : []) }
    func alter(_ changes:Change...) -> Self { changes.reduce(self) { $0.alter($1) }                           }
    var description: String                 { "\(tags.reduce("\n") { $0 + "[\($1)]" }) \(street != nil ? street! : "") \(zip != nil ? zip! : "") \(city != nil ? city! : "") \(country != nil ? country! : "")" }
}
private extension Address {
    private init(_ id:UUID,_ street: String?, _ city: String?,_ country:String?, _ zip: String?, _ tags: [String]) {
        self.id      = id
        self.city    = city
        self.street  = street
        self.country = country
        self.zip     = zip
        self.tags    = tags
    }
    private func alter(_ change:Change) -> Self {
        switch change {
        case let .set(    .street(street )): return .init(id, street, city, country, zip, tags                    )
        case let .set(      .city(city   )): return .init(id, street, city, country, zip, tags                    )
        case let .set(   .country(country)): return .init(id, street, city, country, zip, tags                    )
        case let .set(       .zip(zip    )): return .init(id, street, city, country, zip, tags                    )
        case let .add(       .tag(tag    )): return .init(id, street, city, country, zip, tags + [tag]            )
        case let .remove(    .tag(tag    )): return .init(id, street, city, country, zip, tags.filter({$0 != tag}))
        }
    }
}
