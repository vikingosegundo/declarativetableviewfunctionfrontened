//
//  TableViewHandler.swift
//  FunctionFrontendTableView
//
//  Created by Manuel Meyer on 14.03.22.
//

import UIKit

typealias Command<T> = TableViewHandler<T>.Command<T>

enum TableViewHandler<T> {
    enum Command<T> {
        case register(AnyClass?)
        case data([T])
        case selected((UITableView, UITableViewCell, IndexPath,         T) -> ()             )
        case deleted ((UITableView, UITableViewCell, IndexPath,         T) -> ()             )
        case cell    ((UITableView, UITableViewCell, IndexPath, String, T) -> UITableViewCell)
        case add(datapoint:T)
        case remove(at:IndexPath)
        case get(_Get); enum _Get {
            case tableview((UITableView) -> ())
            case id((String) -> ())
        }
    }
    static func create(for tv:UITableView = UITableView(frame: CGRect.zero)) -> (Command<T>) -> () {
        let ds: DS<T> = DS()
        ds.tableView = tv
        return {
            switch $0 {
            case       .register(let    c ): ds.tableView.register(c, forCellReuseIdentifier:ds.cellID)
            case           .data(let data ): ds.data     = data
            case  .add(datapoint:let    d ): ds.data     = ds.data + [d]
            case      .remove(at:let   ip ): ds.data     = Array(ds.data.enumerated().filter{ i, d in i != ip.row }.map{$1})
            case       .selected(let    s ): ds.selected = s
            case        .deleted(let    d ): ds.deleted  = d
            case           .cell(let    c ): ds.cell     = c
            case .get(.tableview(let  get)): get(ds.tableView)
            case .get(       .id(let  get)): get(ds.cellID)
            }
        }
    }
}

private
extension TableViewHandler {
    typealias TVES = UITableViewCell.EditingStyle
    final class DS<T>: NSObject, UITableViewDataSource, UITableViewDelegate {
        func tableView(_ tableView: UITableView, numberOfRowsInSection   section: Int      ) -> Int             { data.count }
        func tableView(_ tableView: UITableView, cellForRowAt          indexPath: IndexPath) -> UITableViewCell { cell(tableView, tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath),indexPath,cellID ,data[indexPath.row]) }
        func tableView(_ tableView: UITableView, didSelectRowAt        indexPath: IndexPath)                    { selected(tableView, tableView.cellForRow(at: indexPath)! ,indexPath, data[indexPath.row]) }
        func tableView(_ tableView: UITableView, canEditRowAt          indexPath: IndexPath) -> Bool            { true }
        func tableView(_ tableView: UITableView, commit:TVES, forRowAt indexPath: IndexPath) {
            if commit == .delete {
                deleted(tableView, tableView.cellForRow(at: indexPath) ?? UITableViewCell(), indexPath, data[indexPath.row])
                data = data.enumerated().filter{ i, _ in i != indexPath.row }.map{ $1 }
            }
        }
        let cellID = UUID().uuidString
        var tableView: UITableView! { didSet { tableView?.dataSource = self; tableView?.delegate = self } }
        var data     : [T] = []     { didSet { tableView?.reloadData()                                  } }
        var selected : (UITableView, UITableViewCell, IndexPath,         T) -> ()              = { _,_,_,_ in }
        var deleted  : (UITableView, UITableViewCell, IndexPath,         T) -> ()              = { _,_,_,_ in }
        var cell     : (UITableView, UITableViewCell, IndexPath, String, T) -> UITableViewCell = { let c = $0.dequeueReusableCell(withIdentifier: $3, for: $2); _ = $4 ; return c }
    }
}
