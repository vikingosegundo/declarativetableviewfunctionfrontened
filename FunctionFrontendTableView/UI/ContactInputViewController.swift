//
//  ContactInputViewController.swift
//  FunctionFrontendTableView
//
//  Created by Manuel Meyer on 15.03.22.
//

import UIKit

class ContactInputViewController: UIViewController {
    @IBOutlet weak var personalName: UITextField!
    @IBOutlet weak var familyName: UITextField!
    var callback:((Contact) -> ())?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBAction func save(_ sender: Any) {
        callback?(.init().alter(
            .set(.personal(name: personalName.text ?? "")),
            .set(  .family(name:   familyName.text ?? "")))
        )
        self.dismiss(animated:true)
    }
}
