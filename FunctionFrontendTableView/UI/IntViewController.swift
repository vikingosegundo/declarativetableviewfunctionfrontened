//
//  ViewController.swift
//  FunctionFrontendTableView
//
//  Created by Manuel Meyer on 14.03.22.
//

import UIKit
final
class IntViewController: UIViewController {
    var tvhandler: ((Command<Int>) -> ())!
    @IBOutlet var tableView:UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tvhandler = TableViewHandler.create(for:tableView!)
        //              ┌──────────────────── Command<Int>
        //              │      ┌───────────── UITableView
        //              │      │ ┌─────────── UITableViewCell
        //              │      │ │  ┌──────── IndexPath
        //              │      │ │  │  ┌───── String
        //              │      │ │  │  │ ┌─── Int
        tvhandler(.selected({ tv,c,ip,   d in tv.deselectRow(at:ip, animated:true) }))
        tvhandler(    .cell({ tv,c,ip,id,d in populate(tv, c, ip, id, d)           }))
        tvhandler(    .data(                  initialData()                         ))
        tvhandler(.register(UITableViewCell.self))
    }
}
fileprivate
func populate(_ tv: UITableView,
              _ c : UITableViewCell,
              _ ip: IndexPath,
              _ id: String,
              _ d : Int) -> UITableViewCell
{
    c.textLabel?.text="\(d)"
    return c
}
fileprivate
func initialData() -> [Int] { (1 ..< 4).map{$0} }
