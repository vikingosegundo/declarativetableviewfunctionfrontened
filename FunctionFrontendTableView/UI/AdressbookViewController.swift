//
//  AdressbookViewController.swift
//  FunctionFrontendTableView
//
//  Created by Manuel Meyer on 14.03.22.
//

import UIKit
final
class AdressbookViewController: UIViewController {
    var tvhandler: ((Command<Contact>) -> ())!
    @IBOutlet var tableView:UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tvhandler = TableViewHandler.create(for:tableView!)
        //              ┌──────────────────────────────── Command<Contact>
        //              │                  ┌───────────── UITableView
        //              │                  │ ┌─────────── UITableViewCell
        //              │                  │ │  ┌──────── IndexPath
        //              │                  │ │  │  ┌───── String
        //              │                  │ │  │  │ ┌─── Contact
        tvhandler(    .cell({             tv,c,ip,id,d in populate(tv, c, ip, id, d)                                             }))
        tvhandler(.selected({ [weak self] tv,c,ip,   d in showDetail(for:d, on:self)                                             }))
        tvhandler( .deleted({             tv,c,ip,   d in data = Array(data.enumerated().filter{ i,d in i != ip.row }.map{ $1 }) }))
        tvhandler(    .data(                              data                                                                    ))
        tvhandler(.register(UITableViewCell.self))
    }
}
extension AdressbookViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ContactInputViewController {
            vc.callback = { [weak self] in
                   $0.familyName   != ""
                || $0.personalName != ""
                    ? self?.tvhandler(.add(datapoint:$0))
                    : ()
            }
        }
        if
            let vc      = segue.destination as? ContactDetailViewController,
            let contact = sender as? Contact
        {
            vc.contact = contact
        }
    }
}
fileprivate
func showDetail(for contact:Contact, on viewController:AdressbookViewController?) {
    viewController?.performSegue(withIdentifier:"contactDetail", sender:contact)
    viewController?.tableView.indexPathsForSelectedRows?.forEach({
        viewController?.tableView.deselectRow(at:$0, animated:true)
    })
}
fileprivate
func populate(
    _ tv: UITableView,
    _ c : UITableViewCell,
    _ ip: IndexPath,
    _ id: String,
    _ d : Contact
) -> UITableViewCell
{
    c.textLabel?.text="\(d)"
    return c
}
fileprivate
var data = [
    Contact().alter(
        .set(.family  (name:"Meyer")),
        .set(.personal(name:"Manuel"))),
    Contact().alter(
        .set(.family  (name:"Lamarr")),
        .set(.personal(name:"Bruno"))),
    Contact().alter(
        .set(.personal(name:"Bruno"))),
    Contact().alter(
        .set(.family  (name:"Lamarr")))
]
