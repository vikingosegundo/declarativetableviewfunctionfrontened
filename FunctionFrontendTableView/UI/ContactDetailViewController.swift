//
//  ContactDetailViewController.swift
//  FunctionFrontendTableView
//
//  Created by Manuel Meyer on 15.03.22.
//

import UIKit
final
class ContactDetailViewController: UIViewController {
    var contact:Contact?
    override func viewDidLoad() {
        super.viewDidLoad()
        let l = UILabel(frame: .zero)
        l.text = (contact?.familyName ?? "") + (contact?.familyName != nil && contact?.personalName != nil ? ", " : "" ) + (contact?.personalName ?? "")
        l.sizeToFit()
        l.center = view.center
        view.addSubview(l)
    }
}
